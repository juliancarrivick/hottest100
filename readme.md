# Julian's Hottest 100 Shortlister

Takes Last.fm listening data, stores it in SQLite, queries Musicbrainz to get the release date and returns the songs that were released that year.

## Get Started

```
python -m venv .venv
pip install musicbrainzngs
```