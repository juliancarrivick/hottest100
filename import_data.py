import logging
import sqlite3
from typing import NamedTuple
import musicbrainzngs
from difflib import SequenceMatcher

logging.basicConfig(format='%(asctime)s|%(levelname)s|%(message)s', level=logging.INFO)

musicbrainzngs.set_useragent('Hottest100Shortlister', '0.0.1', 'julian@carrivick.com')

class Track(NamedTuple):
    id: int
    artist: str
    album: str
    name: str

class MusicBrainzMetadata(NamedTuple):
    id: str
    released: str

def main():
    conn = sqlite3.connect("data/hottest100.sqlite")

    query_cursor = conn.cursor()
    update_cursor = conn.cursor()

    query_cursor.execute("""
        SELECT Id, Artist, Album, Track
        FROM Tracks
        WHERE MbRecordingId IS NULL
        LIMIT 5
        OFFSET 5
    """)
    rows = query_cursor.fetchall()
    for i, row in enumerate(rows, start=1):
        track = Track(*row)
        logging.info(f"[{i}/{len(rows)}] Searching for {track.name} on {track.album} by {track.artist}")

        if metadata := _get_metadata(track):
            logging.info(f"Released on {metadata.released}")
            update_cursor.execute("""
                UPDATE Tracks SET MbRecordingId = ?, Released = ? WHERE Id = ?
            """, (metadata.id, metadata.released, track.id))
        else:
            logging.info(f"Not found :(")

    conn.commit()
    query_cursor.close()
    update_cursor.close()


def _get_metadata(track: Track) -> MusicBrainzMetadata | None:
    query = f'artist:"{track.artist}" release:"{track.album}" title:"{track.name}"'
    result = musicbrainzngs.search_recordings(query, limit=5)
    recordings = result.get("recording-list", [])
    if not recordings:
        return None

    recordings = sorted(recordings, key=lambda r: SequenceMatcher(None, track.name, r["title"]).ratio())
    recording = recordings[0]
    # release_group = musicbrainzngs.get_release_group_by_id(recording[0]["release_group"]["id"])
    release_dates = [r["date"] for r in recording.get("release-list", []) if "date" in r]
    release_dates = sorted([d for d in release_dates if len(d) == 10])
    if not release_dates:
        return None


    return MusicBrainzMetadata(recording["id"], release_dates[0], recording["ext:score"])

if __name__ == "__main__":
    main()
